# README #

This is the code sample repository for the devEloquence team.
The goal is to give you a feel of our skills and breadth.

If you see anything that you like here, feel free to take them and 
use them in your own applications.


### Main Contact ###

Scott Yang - scottyang (at) develoquence (dot) com