express = require("express")
path = require("path")
favicon = require("static-favicon")
util = require("util")
logger = require("morgan")
cookieParser = require("cookie-parser")
bodyParser = require("body-parser")
routes = require("./routes/index")
user = require("./routes/user")
task = require("./routes/task")
db = require("./models")
app = express()
redisAdapter = require("redis")
memdb = redisAdapter.createClient()

# view engine setup
app.set "views", path.join(__dirname, "views")
app.set "view engine", "jade"
app.use favicon()
app.use logger("dev")
app.use bodyParser.json()
app.use bodyParser.urlencoded()
app.use cookieParser()
app.use express.static(path.join(__dirname, "public"))

# General DB error function
app.use (req, res, next) ->
	res.dbError = (errFields) ->
		errMsg = "DB Error" + ' ' + util.inspect(errFields)
		err = new Error(errMsg)
		err.status = 500
		next err
		return
	next()
	return

# RESTful routes
app.get "/", routes.index
app.get "/staticroute", routes.staticroute
app.post "/users/create", user.create
app.post "/users/:user_id/delete", task.destroy
app.post "/users/:user_id/tasks/create", task.create
app.post "/users/:user_id/tasks/:task_id/delete", task.destroy


#/ catch 404 and forward to error handler
app.use (req, res, next) ->
	err = new Error("Not Found")
	err.status = 404
	next err
	return


#/ error handlers

# development error handler
# will print stacktrace
if app.get("env") is "development"
	app.use (err, req, res, next) ->
		res.status err.status or 500
		res.render "error",
			message: err.message
			error: err
		return


# production error handler
# no stacktraces leaked to user
app.use (err, req, res, next) ->
	res.status err.status or 500
	res.render "error",
		message: err.message
		error: {}
	return


module.exports = app