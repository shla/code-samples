using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ShipMonitor;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Collections;

public partial class _Default : System.Web.UI.Page 
{
    private String m_connStr;
    private String m_logfilename;
    private String m_ConditionStr;
    private String m_strSQL;
    private String m_strSQL_btn;
    public String  m_begindatestr;
    ArrayList m_btns;
    public DateTime m_begin;
    public DateTime m_end;
    private static int m_flag;

    public string GetSelectSQLCaptionForTable(string tablename)
    {
        string strSQL;
        if (tablename == "ship_0001")//三航桩16
        {
            strSQL = "select Convert(varchar(10),RecordTime ,111) as 日期,RYCOilCount as 燃油舱加油量_立方, DZCount    as 打桩数量_根,DZHours    as 打桩时间_小时," +
            "DZOilCount as 打桩工况燃油消耗_立方," +
            "DZPowerCount as 打桩工况耗油率_千克每小时," +
            "StopHours as 抛锚时间_小时," +
            "StopOilCount as 抛锚工况燃油消耗_立方,StopPowerCount as 抛锚工况发电机功率_千瓦时," +
            "SailHours as 移船时间_小时 ,SailOilCount as 移船工况燃油消耗_立方,SailPowerCount as 移船工况耗油率_千克每小时," +
            "KBHours      as 靠泊时间_小时," +
            "RYYGOilCount as 日用油柜累计加油量_立方 from ";

        }
        else
        {            
            strSQL = "select Convert(varchar(10),RecordTime ,111) as 日期,RYCOilCount as 燃油舱加油量_立方, SailMileage    as 航行工况累计里程_海里,DZHours    as 港作时间_小时," +
                    "DZOilCount as 港作工况燃油消耗_立方," +
                    "DZPowerCount as 港作工况耗油率_千克每小时," +
                    "StopHours as 停泊时间_小时," +
                    "StopOilCount as 停泊工况燃油消耗_立方,StopPowerCount as 停泊工况发电机功率_千瓦时," +
                    "SailHours as 航行时间_小时 ,SailOilCount as 航行工况燃油消耗_立方,SailPowerCount as 航行工况耗油率_吨每百海里," +
                    "KBHours      as 靠泊时间_小时," +
                    "RYYGOilCount as 日用油柜累计加油量_立方 from ";
        }
        return strSQL;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        string strSQL;
        string TableName;
        DateTime now=new DateTime();
        now = DateTime.Now;
        /*
         * This part is used to initialized the dateselected. 
         * If this is changed the select control's initialization should be changed also.         
         * 
         */
        
        {
            if (this.dateendselected.Value == "")
            {
                this.dateendselected.Value = now.Year + "/" + now.Month + "/" + now.Day;
                m_end = now;
            }
            now = now.AddMonths(-2);
            if (this.dateselected.Value == "")
            {
                m_begindatestr = this.dateselected.Value = now.Year + "/" + now.Month + "/" + now.Day;
                m_begin = now;
            }
            
        }
        m_btns=new ArrayList();
        m_connStr = "server=vip204.5ghl.cn;uid=a0313175908;pwd=hsship123456;database=a0313175908";
        m_logfilename = Server.MapPath("./App_Data/log.txt");


        if ((Session["stablename"]==null))
        {
            TableName = "ship_0001";
            Response.Cookies["tablename"].Value = TableName;
            Session["stablename"] = TableName;
        }
        else
        {

            if (Session["stablename"]=="")
            {
                TableName = "ship_0001";
                Response.Cookies["tablename"].Value = TableName;
                Session["stablename"] = TableName;
            }
            else
            {

                TableName =(string) Session["stablename"];
            }
        }
        strSQL = GetSelectSQLCaptionForTable(TableName); 
        
        m_strSQL = strSQL;
        CreateButtons();

        strSQL += TableName+ GetTimeRange(); 
       
        SelectNewShipData(strSQL);                
    }

    String GetTimeRange()
    {
        DateTime begin = new DateTime();
        begin = DateTime.Parse(this.dateselected.Value.ToString());
        DateTime end=new DateTime();
        end = DateTime.Parse(this.dateendselected.Value.ToString());

        String strSQLWhere = " where RecordTime >='" + begin.Year.ToString() + "-" + begin.Month.ToString() + "-" + begin.Day.ToString() + "' "
            + " and RecordTime<= '" + end.Year.ToString() + "-" + end.Month.ToString() + "-" + end.Day.ToString() + "' order by RecordTime";

        return strSQLWhere;
    }

    void bt_Click(object sender, EventArgs e)
    {
        int i;
        for(i=0;i<m_btns.Count;i++)
        {
            Button bt=new Button();
            bt=(Button)m_btns[i];
            if(bt!=(Button)sender)
            bt.BackColor = System.Drawing.Color.LightGray;
        }
        ((Button)sender).BackColor = System.Drawing.Color.Blue;

        
        String TableName=  ((Button)sender).ID.ToString();

        Response.Cookies["tablename"].Value = TableName;
        Session["stablename"] = TableName;
        m_begin = DateTime.Parse(this.dateselected.Value.ToString());
        m_end = DateTime.Parse(this.dateendselected.Value.ToString());

        m_strSQL = GetSelectSQLCaptionForTable(TableName); //"select RecordTime as 日期,StopMinutes as 停留时间,SailMinutes as 航行时间,StopOilCount as 停泊工况燃油消耗,SailOilCount as 航行燃油消耗,StopPowerCount as 停泊累计功率,SailMileage as 航行累计里程,RYYGOilCount as 日用油柜加油量,RYCOilCount as 油舱加油量,longitude as 经度,latitude as 纬度 from ";
        SelectNewShipData(m_strSQL+TableName+this.GetTimeRange());
    }

    private void CreateButtons()
    {
        //Get the whole ships number.
        int i;
        int total = 4;
        for (i = 1; i < total; i++)
        {
            TableCell cell = new TableCell();
            TableRow row = new TableRow();
            Button bt = new Button();
            bt.ID = "ship_" + String.Format("{0:D4}",i);
            if (Session["stablename"]!=null)  if(i==1) bt.BackColor = System.Drawing.Color.Blue;
                else if (bt.ID == Session["stablename"]) bt.BackColor = System.Drawing.Color.Blue;

            if (i == 1) bt.Text = "三航桩0016";
            else if (i == 2) bt.Text = "三航拖3003";
            else if (i == 3) bt.Text = "三航拖3005";
            else bt.Text = "船舶" + i.ToString();
            bt.Click += new EventHandler(bt_Click);
            cell.Controls.Add(bt);
            m_btns.Add(bt);
            row.Cells.Add(cell);
            Table1.Rows.Add(row);
        }        
    }
    private void CreateDateTimeSelect()
    {
        Response.Write("<script type=\"text/javascript\">" +
                        "var OneMonthAgo = new Date(); "+     
                        "var ds = new DateSelector(\"idYear\", \"idMonth\", \"idDay\", {"+
                        "Day:OneMonthAgo.getDay(),"+
                        "Month:OneMonthAgo.getMonth(),"+
                        "Year: OneMonthAgo.getFullYear() ,"+
	                    "MinYear: new Date().getFullYear() -1,"+
	                    "MaxYear: new Date().getFullYear()+1 ,"+
	                    "onChange: function(){ document.getElementById(\"dateselected\").value =this.Year + \"/\" + this.Month + \"/\" + this.Day;"+	
	                    "document.getElementById(\"dateendselected\").value =dsend.Year + \"/\" + dsend.Month + \"/\" + dsend.Day;}"+
                        "});"+
                        "ds.onChange();"+
                        "</script>");
    }
    protected void CrystalReportViewer1_Init(object sender, EventArgs e)
    {

    }
    protected void ButtonShip1_Click(object sender, EventArgs e)
    {
        SelectNewShipData("select * from ship_0001");        
    }
    private void SelectNewShipData(String strSQL)
    {
        String tablename = "tablenameIsNULL;";
        DataSet out_ds = new DataSet();
        CMSSQLDatabase db ;
        string retstr;

        
        if(Request.Cookies["tablename"]!=null)  tablename="TN="+Request.Cookies["tablename"].Value;

        
        db= new CMSSQLDatabase(this.m_connStr);
        retstr=db.ExecuteSQL(strSQL, ref out_ds);
        if (retstr.Contains("success"))
        {
            GridView1.DataSource = out_ds;
            GridView1.DataBind();
            TextBoxInfo.Visible = false;
        }
        else
        {           
            GridView1.DataSource = null;
            GridView1.DataBind();
            TextBoxInfo.Text = retstr;
            TextBoxInfo.Visible = true;
        }

        db.Close();
        db.Dispose();
    }
    protected void ButtonShip2_Click(object sender, EventArgs e)
    {
        string strSQL;

        DataSet out_ds = new DataSet();
        CMSSQLDatabase db = new CMSSQLDatabase(this.m_connStr);
        
        strSQL = "select RecordTime as 日期, from ship_0001";

        if (db.ExecuteSQL(strSQL, ref out_ds).Contains("success"))
        {
            GridView1.DataSource = out_ds;
            GridView1.DataBind();
        }
        db.Close();
        db.Dispose();

    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        CMSSQLDatabase db = new CMSSQLDatabase(this.m_connStr);
        String strSQL;
        DataSet ds = new DataSet();
        String retstr;
        int ret;
        int shipid;
        ShipMonitorRecord aRecord = new ShipMonitorRecord();
        /*
        SqlConnection con = new SqlConnection("server=192.168.18.10;uid=sa;pwd=123456;database=hsship");
        
        if (con == null) TextBox2.Text="cannot open database";
        else TextBox2.Text="open database successfully!";
        */
        strSQL = "CREATE TABLE ship_0001 ( ShipID  INTEGER,RecordTime datetime," +
           "StopMinutes  bigint,  SailMinutes  bigint,StopOilCount float," +
            "SailOilCount float,StopPowerCount float,SailMileage float," +
            "RYYGOilCount float,RYCOilCount float," +
            "longitude bigint,latitude bigint)";

        string sqlstring1 = "if not exists(select * from sysobjects where name = 'ship_" + "0001" + "' and type='U') ";

        strSQL = sqlstring1+strSQL;
        retstr=db.ExecuteSQL(strSQL);


        if (retstr.Contains("success") == true)
        {
            //The table ship_0001 is not created.
            //Create the target table.
            //            retstr=db.ExecuteSQL(strSQL,ref ds);

        }
        else CMSSQLDatabase.SaveMessageToLog(retstr, m_logfilename);
        aRecord.SailMileage++;
        strSQL = "Insert into ship_0001 (ShipID,RecordTime," +
          "StopMinutes, SailMinutes,StopOilCount,SailOilCount," +
         "StopPowerCount,SailMileage,RYYGOilCount,RYCOilCount,longitude,latitude ) values (" +
         aRecord.ShipID.ToString() + "," +
         aRecord.RecordTime.ToOADate().ToString() + "," +
         aRecord.StopMinutes.ToString() + "," +
         aRecord.SailMinutes.ToString() + "," +
         aRecord.StopOilCount.ToString() + "," +
         aRecord.SailOilCount.ToString() + "," +
         aRecord.StopPowerCount.ToString() + "," +
         aRecord.SailMileage.ToString() + "," +
         aRecord.RYYGOilCount.ToString() + "," +
         aRecord.RYYGOilCount.ToString() + "," +
         aRecord.longitude.ToString() + "," +
         aRecord.latitude.ToString() + ")";

        aRecord.SailMileage++;
        strSQL += ";Insert into ship_0001 (ShipID,RecordTime," +
          "StopMinutes, SailMinutes,StopOilCount,SailOilCount," +
         "StopPowerCount,SailMileage,RYYGOilCount,RYCOilCount,longitude,latitude ) values (" +
         aRecord.ShipID.ToString() + "," +
         aRecord.RecordTime.ToOADate().ToString() + "," +
         aRecord.StopMinutes.ToString() + "," +
         aRecord.SailMinutes.ToString() + "," +
         aRecord.StopOilCount.ToString() + "," +
         aRecord.SailOilCount.ToString() + "," +
         aRecord.StopPowerCount.ToString() + "," +
         aRecord.SailMileage.ToString() + "," +
         aRecord.RYYGOilCount.ToString() + "," +
         aRecord.RYYGOilCount.ToString() + "," +
         aRecord.longitude.ToString() + "," +
         aRecord.latitude.ToString() + ")";
        retstr=db.ExecuteSQL(strSQL);
        db.Close();
        db.Dispose();
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        

    }
    protected void Button1_Click2(object sender, EventArgs e)
    {
        String TableName = Request.Cookies["tablename"].Value;
        try
        {
            m_begin = DateTime.Parse(this.dateselected.Value.ToString());
            m_end = DateTime.Parse(this.dateendselected.Value.ToString());
            this.SelectNewShipData(m_strSQL +TableName+ GetTimeRange());                
        }
        catch (Exception ex)
        {
            
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {        
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
        m_begin = DateTime.Parse(this.dateselected.Value.ToString());
        m_end = DateTime.Parse(this.dateendselected.Value.ToString());

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {

            //设置gridview头和体不自动换行
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].CssClass = "NoBreak";
            }
        }
    }
}
