package com.MyCom.androidtest;

import java.io.*;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.net.Uri;
import android.content.Intent;
import android.content.Context;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.view.WindowManager.LayoutParams;
import android.util.Base64;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class androidtest extends Activity
{
	// start - static variables
	private static final String LOG_TAG = "MyComandroidTestJava";
	// end - static variables

	// start - debugging variables
	private boolean kp_debug = false;
	// end - debugging variables

	private boolean playing = false;


	public androidtest()
	{
	}


	void write_resource_to_file( int resource_id, String output_fn )
	{
		InputStream  in = null;
		OutputStream out = null;
		try
		{
			in = getResources().openRawResource( resource_id );
			out = new FileOutputStream( output_fn );
			byte[] buffer = new byte[4096];
			int count = in.read( buffer );
			while( count > 0 )
			{
				out.write( buffer, 0, count );
				count = in.read( buffer );
			}
			out.flush();

			Log.i(LOG_TAG, "wrote resource to file");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			Log.e(LOG_TAG, "unexpected exception: " + e.toString());
			e.printStackTrace();
		} finally {

			try {
				if (in != null)
					in.close();
				if (out != null)
					out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

//		write_resource_to_file( R.raw.libandroidbh2test, MyComLibConsts.NATIVE_LIB );
		write_resource_to_file( R.raw.scalingh264, "/data/data/com.MyCom.androidtest/stream" );


		setContentView(R.layout.main);
		SurfaceView surface_view = (SurfaceView) findViewById(R.id.surface_view);
		surface_view.getHolder().addCallback(new SurfaceCallback());

		//
		// Disable screen saver
		//
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	protected void onDestroy() 
	{
		if( playing )
		{
			Log.d( LOG_TAG, "stop playing");
			MyComLib.stopTest();
			playing = false;
		}

		super.onDestroy();
	}


	private final class SurfaceCallback implements SurfaceHolder.Callback
	{
		@Override
		public void surfaceChanged (SurfaceHolder holder, int format, int width, int height)
		{
			if( playing )
			{
				Log.d( LOG_TAG, "stop playing");
				MyComLib.stopTest();
				playing = false;
			}

			Log.d( LOG_TAG, "calling setSurfaceTrans");
			// send surface to JNI to handle at native level
			int rc = MyComLib.setSurfaceTrans( holder.getSurface() );
			if( rc < 0 )
			{
				Log.e( LOG_TAG, "setSurfaceTrans failed: " + rc );
			}

			Log.d( LOG_TAG, "start playing");
			MyComLib.startTest( holder.getSurface() );
			playing = true;
		}
	}
}
