/**
 * Scott Yang
 * 2013-06-19
 */

// Vertical news scroller
var homeNews = {
    newsContent: null,
    newsContentHeight: null,
    newsContentTimer: null,
    newsContentMoverIsOn: 1,
    newsContentMover: function () {
        if (homeNews.newsContentMoverIsOn) {
            var newsContentScrollTop = parseInt(document.getElementById("news-content").style.top);
            if (!newsContentScrollTop) {
                newsContentScrollTop = 0;
            }
            newsContentScrollTop = newsContentScrollTop - 1;
            if (newsContentScrollTop < (0 - homeNews.newsContentHeight)) {
                newsContentScrollTop += homeNews.newsContentHeight;
            }
            if (newsContentScrollTop > 0) {
                newsContentScrollTop -= homeNews.newsContentHeight;
            }
            homeNews.newsContent.css('top', newsContentScrollTop);
        }
        homeNews.newsContentTimer = setTimeout(function () {
            homeNews.newsContentMover()
        }, 80);
    },
    initialize: function () {
        homeNews.newsContent = jQuery("#news-content");
        homeNews.newsContentHeight = homeNews.newsContent.innerHeight();
        var newsContentHTML = homeNews.newsContent.html();
        homeNews.newsContent.html(newsContentHTML + newsContentHTML);
        homeNews.newsContent.mousewheel(function (ev, delta) {
            var newsContentScrollTop = parseInt(homeNews.newsContent.get(0).style.top);
            if (!newsContentScrollTop) {
                newsContentScrollTop = 0;
            }
            newsContentScrollTop = newsContentScrollTop + Math.round(delta * 15);
            if (newsContentScrollTop < (0 - homeNews.newsContentHeight)) {
                newsContentScrollTop += homeNews.newsContentHeight;
            }
            if (newsContentScrollTop > 0) {
                newsContentScrollTop -= homeNews.newsContentHeight;
            }
            homeNews.newsContent.css('top', newsContentScrollTop);
            return false;
        });
        homeNews.newsContent.on("mouseenter", function () {
            homeNews.newsContentMoverIsOn = 0;
        });
        homeNews.newsContent.on("mouseleave", function () {
            homeNews.newsContentMoverIsOn = 1;
        });
        homeNews.newsContentMover();
    }
};