/**
 * Scott Yang
 * 2013-06-19
 */

// Modal alert box and confirm box using jQuery and jQuery UI
function jAlert(msg) {
    jQuery('#jq-dialog-msg').html(msg).dialog({
        modal: true,
        resizable: false,
        buttons: {
            Close: function () {
                $(this).dialog("close");
            }
        }
    });
}

function jConfirm(msg, callback) {
    jQuery('#jq-dialog-msg').html('<img style="float:left;padding: 0 8px 6px 0;" src="/img/warning.png" />' + msg).dialog({
        modal: true,
        resizable: false,
        buttons: {
            Confirm: function () {
                $(this).dialog("close");
                callback();
            },
            Close: function () {
                $(this).dialog("close");
            }
        }
    });
}
