/**
 * Scott Yang
 * 2013-06-19
 */

// Simple cross domain get request opened in a new tab / window
function doImmobelSearch(){
    var location = encodeURIComponent(document.getElementById('ha_location').value);
    var minprice = document.getElementById('ha_minprice').value;
    var maxprice = document.getElementById('ha_maxprice').value;
    var bedroom = document.getElementById('ha_bedroom').value;
    var bathroom = document.getElementById('ha_bathroom').value;
    var link = 'http://www.immobel.com/MRMLS/EN/searchResults.do?glexMarket=&la=EN&per=MRMLS&shcu=&mode=std&rpp=15&c_sort=li_sort_priced&c_location=' + location + '&c_itype=1&minprice=' + minprice + '&maxprice=' + maxprice + '&cu=USD&minbedroom=' + bedroom + '&minbathroom=' + bathroom + '&minsurface=&maxsurface=&c_surface_mu=1&minlsurface=&maxlsurface=&c_lsurface_mu=1&c_mls=';

    window.open(link, '_blank');
}