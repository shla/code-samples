/**
 * Scott Yang
 * 2013-06-19
 */

// Converts seconds to hh:mm:ss
function sec2hms(sec) {
    var hms, hh, mm, ss;
    if (sec < 0) {
        hms = "00:00:00";
        return hms;
    }
    hh = Math.floor(sec / 3600);
    sec = sec % 3600;
    mm = Math.floor(sec / 60);
    ss = Math.floor(sec % 60);
    if (hh < 10)
        hh = "0" + hh;
    if (mm < 10)
        mm = "0" + mm;
    if (ss < 10)
        ss = "0" + ss;
    hms = hh + ":" + mm + ":" + ss;
    return hms;
}