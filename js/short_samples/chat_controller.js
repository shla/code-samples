/**
 * Scott Yang
 * 2012-03-01
 */

// In-Game JS Chat Controller
var chatController = {
    config_x: 0,
    config_y: 0,
    config_width: 200,
    config_height: 150,
    connId: 0,
    pauseScroll: 0,
    ss: 'chat_controller.php',
    execAjax: function (url, params, custCallback) {
        $.ajax({
            type: "POST",
            url: url,
            data: params,
            dataType: "json",
            success: function (resp) {
                if (resp.success) {
                    if (typeof(custCallback) === 'function') {
                        custCallback(resp);
                    }
                } else {
                    if (typeof(resp.disconnect) !== 'undefined' && resp.disconnect) {
                        $('#chatbox_new_msg').prop('disabled', true).val('');
                        $('#chatbox_send').prop('disabled', true);
                    }
                }
                if (typeof(resp.msg) !== 'undefined' && resp.msg) {
                    chatController.showMsg(0, 0, 'System', resp.msg);
                }
            },
            error: function () {
                chatController.showMsg(0, 0, 'System', 'Disconnected.');
            }
        });
    },
    init: function () {
        var minHeight = 150;
        var minWidth = 200;
        var inputHeight = 68;
        $('#chatbox_ctrl_ns').addClass('hidden');
        $('#chatbox_ctrl_norm').addClass('hidden');
        $('#eos_stats_panel .show_chat_control').on('click', function () {
            if (!$('#chatbox').dialog('isOpen')) {
                chatController.init();
            }
        });

        var data = '<div id="chatbox_ma" style="height:' + (Math.max(minHeight, chatController.config_height) - inputHeight - 40) + 'px;"></div><div class="chatbox_new_msg_area"><input id="chatbox_new_msg" type="text" class="bigger_input" maxlength="240" /><input id="chatbox_send" type="button" value="Send" /></div>';
        $('#chatbox').html(data).dialog({
            modal: false,
            position: [chatController.config_x, chatController.config_y],
            minWidth: minWidth,
            minHeight: minHeight,
            width: Math.max(chatController.config_width, minWidth),
            title: 'Chat',
            zIndex: 9901,
            resizable: true,
            create: function (event, ui) {
                $("#chatbox").prev('div.ui-dialog-titlebar').css({padding: '0 1em'});
                $(event.target).parent().css('position', 'fixed');
            },
            open: function (event, ui) {
                $('#chatbox').parent('div.ui-dialog').height(Math.max(chatController.config_height, minHeight));
                $("#chatbox").height(Math.max(chatController.config_height, minHeight));
                $("#chatbox_ma").height($('#chatbox').height() - inputHeight);
                $("#chatbox_ma").width($('#chatbox').width() - 12);
                $("#chatbox_new_msg").width($('#chatbox').width() - 68).prop('disabled', true);
                $('#chatbox_send').prop('disabled', true);
                chatController.connect();
            },
            close: function (event, ui) {
                $('#chatbox_new_msg').prop('disabled', true).val('');
                $('#chatbox_send').prop('disabled', true);
                chatController.disconnect();
                $('#chatbox_ctrl_ns').removeClass('hidden');
                $('#chatbox_ctrl_norm').removeClass('hidden');
            },
            resize: function (event, ui) {
                $("#chatbox").height($('#chatbox').parent('div.ui-dialog').height());
                $("#chatbox_ma").height($('#chatbox').height() - inputHeight);
                $("#chatbox_ma").width($('#chatbox').width() - 12);
                $("#chatbox_new_msg").width($('#chatbox').width() - 68);
            },
            resizeStop: function (event, ui) {
                var position = [(Math.floor(ui.position.left) - $(window).scrollLeft()), (Math.floor(ui.position.top) - $(window).scrollTop())];
                $(event.target).parent().css('position', 'fixed');
                $('#chatbox').dialog('option', 'position', position);
                chatController.updatePosition();
            },
            dragStop: function (event, ui) {
                chatController.updatePosition();
            }
        });
    },
    connect: function () {
        chatController.showMsg(0, 0, 'System', 'Connecting to chat...');
        var params = {action: 'connect'};
        this.execAjax(this.ss, params, function (resp) {
            chatController.showMsg(0, 0, 'System', 'Connected.');
            playerController.playerId = resp.player_id;
            playerController.playerName = resp.player_name;
            $('#chatbox_new_msg').prop('disabled', false);
            $('#chatbox_send').prop('disabled', false);
            $('#chatbox_ma').on('mouseenter', function () {
                chatController.pauseScroll = 1;
            });
            $('#chatbox_ma').on('mouseleave', function () {
                chatController.pauseScroll = 0;
            });
            chatController.connId = resp.conn_id;
            $('#chatbox_new_msg').on('keypress', function (e) {
                if (e.which == 13) {
                    chatController.sendMsg(0);
                }
            });
            $('#chatbox_send').on('click', function () {
                chatController.sendMsg(0);
            });
            chatController.getMsgs(0, 0);
        });
    },
    disconnect: function () {
        var params = {action: 'disconnect', conn_id: chatController.connId};
        this.execAjax(this.ss, params);
        chatController.connId = 0;
        chatController.showMsg(0, 0, 'System', 'Disconnected.');
    },
    updatePosition: function () {
        var absPosition = $('#chatbox').parent('div.ui-dialog').position();
        chatController.config_x = Math.floor(absPosition.left - $(window).scrollLeft());
        chatController.config_y = Math.floor(absPosition.top - $(window).scrollTop());
        chatController.config_height = $('#chatbox').height();
        chatController.config_width = $('#chatbox').width();
        var params = {
            action: 'update_position',
            left: chatController.config_x,
            top: chatController.config_y,
            height: chatController.config_height,
            width: chatController.config_width
        };
        this.execAjax(this.ss, params);
    },
    showMsg: function (msg_id, player_id, player_name, msg_body) {
        if (player_id == 0) {
            $('#chatbox_ma').append('<div style="color:#ff0000;">' + player_name + ': ' + msg_body + '</div>');
        } else {
            $('#chatbox_ma').append('<div id="chat_msg_' + msg_id + '"><a href="/eos/player/' + player_id + '" target="_blank">' + player_name + '</a>: ' + msg_body + '</div>');
        }
    },
    getMsgs: function (chan_id, st) {
        if (typeof(document.getElementById('chatbox_ma')) === 'undefined' || document.getElementById('chatbox_ma') === null) {
            return false;
        }
        var params = {action: 'get_msgs', conn_id: chatController.connId, chan_id: chan_id, st: st};
        this.execAjax(this.ss, params, function (resp) {
            setTimeout('chatController.getMsgs(' + chan_id + ', ' + resp.curr_time + ');', 1500);
            if (resp.msgs.length) {
                for (var i = 0; i < resp.msgs.length; i++) {
                    msg = resp.msgs[i];
                    if (typeof(document.getElementById('chat_msg_' + msg['id'])) === 'undefined' || document.getElementById('chat_msg_' + msg['id']) === null) {
                        chatController.showMsg(msg['id'], msg['player_id'], msg['player_name'], msg['body']);
                    }
                }
                if (!chatController.pauseScroll) {
                    $('#chatbox_ma').animate({scrollTop: $('#chatbox_ma')[0].scrollHeight - $('#chatbox_ma').height()}, 500);
                }
            }
        });
    },
    sendMsg: function (chan_id) {
        var msg_body = $('#chatbox_new_msg').val();
        $('#chatbox_new_msg').val('');
        if (msg_body == '') return false;
        var params = {action: 'send_msg', conn_id: chatController.connId, chan_id: chan_id, msg_body: msg_body}
        chatController.execAjax(chatController.ss, params, function (resp) {
            chatController.showMsg(resp.msg_id, playerController.playerId, playerController.playerName, msg_body);
        });
    }
}
