<?php
/**
 * User: Scott
 * Date: 2013-07-12
 */

/**
 * Minimal static PDO factory, with credentials defined inline
 */
class ScottDBFactory
{
	private static $dbhost;
	private static $dbuser;
	private static $dbpass;
	private static $dbname;
	private static $environment;

	/**
	 * Configurations
	 */
	public static function config()
	{
		static::$dbhost['dev'] = 'localhost';
		static::$dbuser['dev'] = 'simple_notes';
		static::$dbpass['dev'] = 'simple_notes';
		static::$dbname['dev']['main'] = 'simple_notes';

		static::$dbhost['prod'] = 'localhost'; // May need to use outside domain/IP for some server configs
		static::$dbuser['prod'] = 'remote_host';
		static::$dbpass['prod'] = 'remote_pass';
		static::$dbname['prod']['main'] = 'simple_notes';

		if (!static::$environment) {
			if ($_SERVER["SERVER_NAME"] == "localhost") {
				static::$environment = 'dev';
			} elseif ($_SERVER["SERVER_NAME"] == "www.example.com" || $_SERVER["SERVER_NAME"] == "123.45.67.89") {
				static::$environment = 'prod';
			} else {
				die("DB Login is not configured, quitting...");
			}
		}
	}

	/**
	 * Static PDO factory
	 *
	 * @param string $db_alias Database to connect to, as alias as defined in config
	 * @param string|null $environment Server environment, if not provided, will be automatically selected from config
	 * @return PDO
	 */
	public static function connect($db_alias = 'main', $environment = null)
	{
		static::config();
		if (is_null($environment)) $environment = static::$environment;

		// Undefined DB, something is wrong
		if (!isset(static::$dbname[$environment][$db_alias])) die("DB Name is not configured, quitting...");

		$db = new PDO('mysql:host=' . static::$dbhost[$environment] . ';dbname=' . static::$dbname[$environment][$db_alias] . ';charset=utf8', static::$dbuser[$environment], static::$dbpass[$environment]) or die ('Error connecting to DB using PDO');
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
//		$db->query("SET time_zone = 'America/Los_Angeles'");
		$db->query("SET CHARACTER SET utf8");

		return $db;
	}
}