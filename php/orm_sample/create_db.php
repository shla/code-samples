<?php
error_reporting(E_ALL);

require(realpath($_SERVER["DOCUMENT_ROOT"]).'/config/dbconn.php');
$db = ScottDBFactory::connect();

$sql = "CREATE TABLE IF NOT EXISTS `notes` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `date` DATE NOT NULL,
  `body` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
$success = $db->query($sql);

if($success){
	header("Refresh:5;url=index.php");
	echo '<pre>';
	echo 'Table "notes" created:', PHP_EOL;	
	$sql = "DESCRIBE notes";
	$results = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
	foreach($results as $result){
		echo $result['Field'] .' - '. $result['Type'], PHP_EOL;
	}
	echo 'Page will redirect in 5 seconds.', PHP_EOL;
	echo '</pre>';
}