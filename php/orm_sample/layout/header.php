<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Simple Notes</title>
		<meta name="description" content="A simple note taking app">
		<link rel="stylesheet" href="/css/redmond/jquery-ui-1.10.2.min.css">
		<link rel="stylesheet" href="/css/main.css">
	</head>
	<body id="simple_notes_page">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery-1.9.1.min.js"><\/script>')</script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
		<script>jQuery.ui || document.write('<script src="/js/jquery-ui-1.10.2.min.js"><\/script>')</script>
		<script src="/js/moment.min.js"></script>
		<script src="/js/main.js"></script>
		<div id="simple_notes_header">
			<div class="title">Simple Notes</div>
			<div class="message">Just a simple note taking application</div>
		</div>
		<div id="simple_notes_body">