<?php
/**
 * User: Scott
 * Date: 2013-07-12
 */
require_once(ABS_PATH.'/models/generic_orm.php');

// Generic Model
abstract class GenericModel{
	public $orm;
	public $old_data;

	public function __construct($table_name){
		$this->orm = new GenericORM($this, $table_name);
	}
	public function all(){
		$results = $this->orm->getTableData();
		return $results;
	}
	public function count($params = null){
		if(empty($params) || $params == 'all'){
			$result = $this->orm->getCount();
		}else{
			$result = $this->orm->getCount($params);
		}
		return $result;
	}
	public function find($params = null, $sort_query = null, $limit_query = null){
		if(empty($params) || $params == 'all'){
			$results = $this->orm->getTableData(null, $sort_query, $limit_query);
		}else if(is_numeric($params)){
			$params_by_pkey = array($this->orm->getPrimaryKey() => $params);
			$results = $this->orm->getTableData($params_by_pkey, $sort_query, $limit_query);
		}else{
			$results = $this->orm->getTableData($params, $sort_query, $limit_query);
		}
		return $results;
	}
	public function save(){
		if(empty($this->old_data) || is_null($this->old_data->{$this->orm->getPrimaryKey()})){
			$success = $this->orm->insertTableData();
		}else{
			$success = $this->orm->updateTableData();
		}
		return $success;
	}
	public function delete($params = null){
		if(is_numeric($params)){
			$params_by_pkey = array($this->orm->getPrimaryKey() => $params);
			$success = $this->orm->deleteTableData($params_by_pkey);
		}else{
			$success = $this->orm->deleteTableData($params);
		}
		return $success;
	}
}