<?php
/**
 * User: Scott
 * Date: 2013-07-12
 */
require_once(ABS_PATH . '/config/dbconn.php');

// Simple Generic ORM
class GenericORM
{
	public $db;
	private $model;
	public $table_name;
	public $table_columns = array();
	public $primary_key;

	public function __construct($model, $table_name){
		$this->model = $model;
		$this->db = ScottDBFactory::connect();
		$this->table_name = $table_name;
		$this->mapMe($table_name);
	}

	/**
	 * @return PDO
	 */
	public function getDB(){
		return $this->db;
	}

	private function getModel(){
		return $this->model;
	}

	public function mapMe($table_name)
	{
		$query = $this->getDB()->prepare("SHOW COLUMNS FROM $table_name");
		$success = $query->execute();
		if (!$success) {
			throw new Exception('Could not run query: ' . print_r($this->getDB()->errorInfo()));
		} else {
			$columns = $query->fetchAll(PDO::FETCH_ASSOC);
			foreach ($columns as $col) {
				$this->getModel()->$col['Field'] = null;
				$this->table_columns[] = $col['Field'];
				if ($col['Key'] == 'PRI') $this->primary_key = $col['Field'];
			}
		}
		return $this;
	}

	public function getPrimaryKey()
	{
		return $this->primary_key;
	}

	public function getCount($params = null)
	{
		if (empty($params)) {
			$sql = "SELECT COUNT(*) FROM {$this->table_name}";
			$query = $this->getDB()->query($sql);
			$results = $query->fetchColumn();
		} else {
			$sql = "SELECT COUNT(*) FROM {$this->table_name} WHERE ";
			$sql_where = array();
			$binded_params = array();
			foreach ($params as $key => $val) {
				if (is_array($val)) {
					$placeholder = array_fill(0, count($val), '?');
					$sql_where[] = "`$key` IN (" . implode(',', $placeholder) . ")";
					$binded_params = array_merge($binded_params, $val);
				} else {
					$sql_where[] = "`$key` = ?";
					$binded_params[] = $val;
				}
			}
			$sql .= implode(' AND ', $sql_where);
			$query = $this->getDB()->prepare($sql);
			$query->execute($binded_params);
			$results = $query->fetchColumn();
		}
		if (!$query) {
			echo 'Could not run query: ' . print_r($this->getDB()->errorInfo());
		}
		return (int)$results;
	}

	public function getTableData($params = null, $sort_query = null, $limit_query = null)
	{
		if (empty($params)) {
			$sql = "SELECT * FROM {$this->table_name}";
			if ($sort_query) $sql .= " ORDER BY $sort_query";
			if ($limit_query) $sql .= " LIMIT $limit_query";
			$query = $this->getDB()->query($sql);
			$results = $query->fetchAll(PDO::FETCH_OBJ);
		} else {
			$sql = "SELECT * FROM {$this->table_name} WHERE ";
			$sql_where = array();
			$binded_params = array();
			foreach ($params as $key => $val) {
				if (is_array($val)) {
					$placeholder = array_fill(0, count($val), '?');
					$sql_where[] = "`$key` IN (" . implode(',', $placeholder) . ")";
					$binded_params = array_merge($binded_params, $val);
				} else {
					$sql_where[] = "`$key` = ?";
					$binded_params[] = $val;
				}
			}
			$sql .= implode(' AND ', $sql_where);
			if ($sort_query) $sql .= ' ORDER BY $sort_query';
			if ($limit_query) $sql .= ' LIMIT $limit_query';
			$query = $this->getDB()->prepare($sql);
			$query->execute($binded_params);
			$results = $query->fetchAll(PDO::FETCH_OBJ);
		}
		if (!$query) {
			echo 'Could not run query: ' . print_r($this->getDB()->errorInfo());
			exit();
		}

		if (count($results) == 1) {
			// Map object
			$this->getModel()->old_data = new stdClass();
			foreach ($this->table_columns as $col) {
				$this->getModel()->$col = $results[0]->$col;
				$this->getModel()->old_data->$col = $results[0]->$col;
			}
		}

		return $results;
	}

	public function insertTableData()
	{
		$params = array(); // Insert only changed columns
		foreach ($this->table_columns as $col) {
			if ($this->getModel()->$col !== null) {
				$params['`' . $col . '`'] = $this->getModel()->$col;
			}
		}
		if (empty($params)) return true; // Success on no change
		$cols = implode(',', array_keys($params));
		$placeholder = implode(',', array_fill(0, count($params), '?'));
		$data = array_values($params);
		$query = $this->getDB()->prepare("INSERT INTO {$this->table_name} ($cols) VALUES ($placeholder)");
		$result = $query->execute($data);

		// Remove data after save
		foreach ($this->table_columns as $col) {
			$this->getModel()->$col = null;
		}

		return $result;
	}

	public function updateTableData()
	{
		$primary_key = $this->getModel()->old_data->{$this->primary_key};
		$params = array(); // Update only changed columns
		foreach ($this->table_columns as $col) {
			if ($this->getModel()->$col !== $this->getModel()->old_data->$col) {
				$params['`' . $col . '`'] = $this->getModel()->$col;
			}
		}
		if (empty($params)) return true; // Success on no change
		$cols_to_update = array();
		foreach ($params as $key => $val) {
			$cols_to_update[] = $key . ' = ?';
		}
		$set_query = implode(',', $cols_to_update);
		$data = array_values($params);
		$query = $this->getDB()->prepare("UPDATE {$this->table_name} SET $set_query WHERE `{$this->primary_key}` = $primary_key");
		$result = $query->execute($data);

		// Remove old data after update
		$this->getModel()->old_data = null;

		// Remove data after save
		foreach ($this->table_columns as $col) {
			$this->getModel()->$col = null;
		}

		return $result;
	}

	public function deleteTableData($params = null)
	{
		if (empty($params)) {
			if (empty($this->getModel()->old_data) || is_null($this->getModel()->old_data->{$this->primary_key})) return false; // Nothing to delete
			$primary_key = $this->getModel()->old_data->{$this->primary_key};
			$query = $this->getDB()->prepare("DELETE FROM {$this->table_name} WHERE `{$this->primary_key}` = $primary_key");
			$result = $query->execute();
		} else {
			$sql = "DELETE FROM {$this->table_name} WHERE ";
			$sql_where = array();
			$binded_params = array();
			foreach ($params as $key => $val) {
				if (is_array($val)) {
					$placeholder = array_fill(0, count($val), '?');
					$sql_where[] = "`$key` IN (" . implode(',', $placeholder) . ")";
					$binded_params = array_merge($binded_params, $val);
				} else {
					$sql_where[] = "`$key` = ?";
					$binded_params[] = $val;
				}
			}
			$sql .= implode(' AND ', $sql_where);
			$query = $this->getDB()->prepare($sql);
			$result = $query->execute($binded_params);
		}
		if ($result) {
			// Remove data after delete
			$this->getModel()->old_data = null;
			foreach ($this->table_columns as $col) {
				$this->getModel()->$col = null;
			}
		}

		return $result;
	}

	public function testShowTable($table_name = null)
	{
		// Test function to dump table cols - not a part of the actual program
		if (is_null($table_name)) return false;

		$table_name = filter_var($table_name, FILTER_SANITIZE_STRING);
		$query = $this->getDB()->prepare("SHOW COLUMNS FROM $table_name");
		$success = $query->execute();
		if (!$success) {
			throw new Exception('Could not run query: ' . print_r($this->getDB()->errorInfo()));
		} else {
			$columns = $query->fetchAll(PDO::FETCH_ASSOC);
			foreach ($columns as $col) {
				var_dump($col);
			}
		}

		return true;
	}
}