<?php
/**
 * User: Scott
 * Date: 2013-07-12
 */
require_once(ABS_PATH . '/models/generic_model.php');

/**
 * Simple Notes ORM
 */
class Note extends GenericModel
{
	public function __construct()
	{
		$this->table_name = 'notes';
		parent::__construct($this->table_name);
	}
}