<?php
/**
 * User: Scott
 * Date: 2013-06-19
 */

// Excerpt from a controller file on a non-framework based custom site
if($action == 'delete_property'){
	$property_id = filter_var($_POST['property_id'], FILTER_SANITIZE_NUMBER_INT);

	// Delete property listing
	$query = $db->prepare("DELETE FROM properties WHERE id = :pid");
	$query->execute(array(':pid' => $property_id));
	$affected = $query->rowCount();
	if(!$affected){
		$resp = array('success' => 0, 'msg' => 'Property not found.');
		echo json_encode($resp);
		exit;
	}

	// Delete all image files belonging to the listing
	$sql = "SELECT filename FROM properties_images WHERE property_id = $property_id";
	$img_files = $db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
	foreach($img_files as $img_file){
		$filename = $img_file['filename'];
		@unlink('../img/properties/'.$filename);
		@unlink('../img/properties/thumbs/'.$filename);
	}

	// Finally remove the images in the db
	$sql = "DELETE FROM properties_images WHERE property_id = $property_id";
	$db->query($sql);

	echo json_encode(array('success' => 1));
	exit;
}