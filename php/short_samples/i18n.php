<?php
/**
 * User: Scott
 * Date: 2013-06-19
 */

// A short language switcher function
session_start();
if(isset($_POST['q']) && $_POST['q'] !== ''){
	$q = filter_var($_POST['q'], FILTER_SANITIZE_STRING);
	$q_options = array('en-US', 'es-MX', 'fr-FR', 'zh-CN', 'zh-TW');
	if(in_array($q, $q_options)){
		$_SESSION['lang'] = $q;
		$resp = array('success' => 1);
		echo json_encode($resp);
	}else{
		$resp = array('success' => 0, 'msg' => 'Invalid Language');
	}
}else{
	$resp = array('success' => 0, 'msg' => 'No Language Given');
}

// The set locale portion to match the above sample (on a different page)
$locale = isset($_SESSION['lang']) ? $_SESSION['lang'] : 'en-US';
$locale_f = preg_replace('/[-]+/', '_', $locale);
putenv("LC_ALL=$locale_f");
setlocale(LC_ALL, $locale_f);
bindtextdomain("default", realpath($_SERVER["DOCUMENT_ROOT"])."/locale");
bind_textdomain_codeset("default", 'UTF-8');
textdomain("default");