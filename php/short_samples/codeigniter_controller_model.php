<?php
/**
 * User: Scott
 * Date: 2013-06-19
 */

// CodeIgnitor Controller Sample
class Bucket extends Custom_Controller{
	function __construct(){
		parent::__construct();
		if(!sess_var('logged_in')){
			redirect('login');
		}
		$this->load->model('buckets_model');
	}
	function fill(){
		$params = $this->uri->uri_to_assoc(3);
		if(!isset($params['bucket_id']) || !$params['bucket_id']){
			$resp = array('success' => 0, 'msg' => 'Bucket ID is missing.');
			echo json_encode($resp);
			return;
		}
		if(!isset($params['water_vol']) || $params['water_vol'] < 0 || $params['water_vol'] > 100){
			$resp = array('success' => 0, 'msg' => 'Water volume is missing or is invalid.');
			echo json_encode($resp);
			return;
		}
		$new_water_vol = $this->buckets_model->fill_bucket($params['bucket_id'], $params['water_vol']);
		$resp = array('success' => 1, 'bucket_id' => $params['bucket_id'], 'water_vol' => $new_water_vol);
		echo json_encode($resp);
		return;
	}
	function empti($bucket_id=0){
		if(!$bucket_id){
			$resp = array('success' => 0, 'msg' => 'Bucket ID is missing.');
			echo json_encode($resp);
			return;
		}
		$this->buckets_model->empty_bucket($bucket_id);
		$resp = array('success' => 1, 'bucket_id' => $bucket_id, 'water_vol' => 0);
		echo json_encode($resp);
		return;
	}
}

// CodeIgnitor Model Sample
class Buckets_model extends CI_Model {
	function fill_bucket($bucket_id, $water_vol){
		// Add water
		$sql = "UPDATE buckets SET water_vol = water_vol + ? WHERE id = ?";
		$this->db->query($sql, array($water_vol, $bucket_id));

		// Return new water level
		$new_water_vol = $this->db->get_where('buckets', array('id', $bucket_id))->row()-> water_vol;
		return $new_water_vol;
	}
	function empty_bucket($bucket_id){
		$this->db->where('id', $bucket_id)
			->set('water_vol', 0)
			->update('buckets');
		return 1;
	}
}