// Generated by CoffeeScript 1.7.1
(function() {
  module.exports = function(sequelize, DataTypes) {
    var User;
    User = sequelize.define("User", {
      username: {
        type: DataTypes.STRING,
        validate: {
          isAlpha: true
        }
      }
    }, {
      classMethods: {
        associate: function(models) {
          User.hasMany(models.Book);
        }
      }
    });
    return User;
  };

}).call(this);

//# sourceMappingURL=user.map
