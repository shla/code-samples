module.exports = (sequelize, DataTypes) ->
	User = sequelize.define("User",
		username:
			type: DataTypes.STRING
			validate:
				isAlpha: true
	,
		classMethods:
			associate: (models) ->
				User.hasMany models.Book
				return
	)
	User